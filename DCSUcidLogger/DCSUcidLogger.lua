local ucidLogger = {}

function ucidLogger.onPlayerConnect(id)
    log.write('UcidLogger', log.INFO, 'Connected client! id: [' .. id .. '] name: [' .. net.get_player_info(id, 'name') .. '] ucid: [' .. net.get_player_info(id, 'ucid') .. ']');
end

DCS.setUserCallbacks(ucidLogger)
log.write('UcidLogger', log.INFO, 'UcidLogger loaded.')

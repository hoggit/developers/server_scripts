local suWhitelist = {}

suWhitelist.freeAirframes = {
    "Su-25T",
    "TF-51D"
}

suWhitelist.gameMasterTag = {
    "instructor"
}

suWhitelist.config = {}
suWhitelist.config.enabled = true

suWhitelist.clients_file = lfs.writedir() .. [[whitelist-clients.lua]]
suWhitelist.gamemaster_file = lfs.writedir() .. [[gamemaster-clients.lua]]

suWhitelist.failedSlots_file = lfs.writedir() .. [[failed-slots.txt]]
suWhitelist.failedSlots = {}

suWhitelist.blockAirframes = {}
suWhitelist.whitelist = {}
suWhitelist.gamemasters = {}

net.log("Whitelist.lua ---- Loading")

io.open(suWhitelist.failedSlots_file,"w"):close()

suWhitelist.checkAirframe = function(slotId)
    local slotType = DCS.getUnitType(slotId)
    for _,a in pairs(suWhitelist.blockAirframes) do
        if slotType == a then 
            net.log("Restricted airframe found")
            return true
        end
    end
    return false
end

suWhitelist.gameMasterCheck = function(slotId)
    local slotType = DCS.getUnitType(slotId)
    for _,a in pairs(suWhitelist.gameMasterTag) do
        if slotType == a then
            net.log("Attempting to access game master slot")
            return true
        end
    end
    return false
end

suWhitelist.loadClients = function()
    local fileData = dofile(suWhitelist.clients_file)
    suWhitelist.whitelist = {}
    for _,c in pairs(fileData) do
        table.insert(suWhitelist.whitelist, c)
    end

    local gmData = dofile(suWhitelist.gamemaster_file)
    suWhitelist.gamemasters = {}
    for _,c in pairs(gmData) do
        table.insert(suWhitelist.gamemasters, c)
    end

end

suWhitelist.getPlayerUcid = function(playerId)
    return net.get_player_info(playerId, 'ucid')
end

suWhitelist.getPlayerName = function(playerId)
    return net.get_player_info(playerId, 'name')
end

suWhitelist.playerInWhitelist = function(playerId)
    local playerUcid = suWhitelist.getPlayerUcid(playerId)
    net.log("Checking ["..playerUcid.."] against whitelist")
    for _,p in pairs(suWhitelist.whitelist) do
        if playerUcid == p then return true end
    end
    return false
end

suWhitelist.playerInGMList = function(playerId)
    local playerUcid = suWhitelist.getPlayerUcid(playerId)
    net.log("Checking ["..playerUcid.."] against game master list")
    for _,p in pairs(suWhitelist.gamemasters) do
        if playerUcid == p then return true end
    end
    return false
end

suWhitelist.playerInFailedList = function(playerUcid)
    for _,p in pairs(suWhitelist.failedSlots) do
        if playerUcid == p then return true end
    end
    return false
end

suWhitelist.nameIsEmpty = function(n)
  return n == nil or n == '' or n == 'Player'
end

suWhitelist.writeFailedSlot = function(playerId)
    local playerUcid = suWhitelist.getPlayerUcid(playerId)
    local playerInFailedList = suWhitelist.playerInFailedList(playerUcid)
    local playerName = suWhitelist.getPlayerName(playerId)
    if not playerInFailedList and not suWhitelist.nameIsEmpty(playerName) then
        net.log("suWhitelist - Writing to file: " .. suWhitelist.failedSlots_file)
        local failed_Slots = io.open(suWhitelist.failedSlots_file, 'a')
        failed_Slots:write(playerUcid .. "," .. playerName .. '\n')
        failed_Slots:close()
        table.insert(suWhitelist.failedSlots, playerUcid)
    end
end

suWhitelist.playerEnterUnit = function(playerId, slotId)
    if suWhitelist.gameMasterCheck(slotId) == true then
        suWhitelist.loadClients()
        local playerInGMList = suWhitelist.playerInGMList(playerId)
        if not playerInGMList then
            net.send_chat_to(suWhitelist.gmMessage, playerId, playerId)
            net.force_player_slot(playerId, 0, '')
            return
        end
    end

    if suWhitelist.checkAirframe(slotId) == false then return end
    suWhitelist.loadClients()
    local playerInWhitelist = suWhitelist.playerInWhitelist(playerId)
    if not playerInWhitelist then
        net.send_chat_to(suWhitelist.message, playerId, playerId)
        net.force_player_slot(playerId, 0, '')
        suWhitelist.writeFailedSlot(playerId)
    end
end

suWhitelist.message = "ERROR: use !slotaccess on discord and follow the instructions to get access to this slot"
suWhitelist.gmMessage = "ERROR: Staff/dev only"

suWhitelist.eventHandler = {}

suWhitelist.eventHandler.onGameEvent = function(eventName, playerId, slotId)
    local status, err = pcall(function(_event)
        if eventName == "change_slot" and slotId ~= "" then
            suWhitelist.playerEnterUnit(playerId, slotId)
        end
    end, _event)
end

suWhitelist.eventHandler.onMissionLoadEnd = function(newMiz)
    t = net.dostring_in("mission", "return mission.theatre" )
    net.log("Whitelist.lua ---- Terrain is ["..t.."]")
    if not suWhitelist.config.enabled then
        net.log("Whitelist.lua ---- Whitelist disabled, free slots open")
        suWhitelist.blockAirframes = {}
    elseif t == "Caucasus" then 
        net.log("Whitelist.lua ---- Free map detected, locking free slots")
        suWhitelist.blockAirframes = suWhitelist.freeAirframes
    elseif t == "MarianaIslands" then 
        net.log("Whitelist.lua ---- Free map detected, locking free slots")
        suWhitelist.blockAirframes = suWhitelist.freeAirframes
    else 
        net.log("Whitelist.lua ---- Paid map detected, free slots open")
        suWhitelist.blockAirframes = {}
    end
end

DCS.setUserCallbacks(suWhitelist.eventHandler)
net.log("Whitelist.lua ---- Loaded")

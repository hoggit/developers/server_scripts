#Requires -RunAsAdministrator
$SRSPath = "C:\Users\Public\chef_files\SRS_Server"
$DCSPath = "C:\Program Files\Eagle Dynamics\DCS World OpenBeta Server"
$DCSSavedPath = "C:\Users\hoggit\saved games\dcs.openbeta_server"

if ($env:DCSPATH) {
    $DCSPath = $env:DCSPATH
}

if ($env:DCSSAVEDPATH) {
    $DCSSavedPath = $env:DCSSAVEDPATH
}

if ($env:SRSPATH) {
    $SRSPath = $env:SRSPATH
}

if ($env:DCSADMINTOOLSPATH) {
    $adminToolsPath = $env:DCSADMINTOOLSPATH
}
else {
    $adminToolsPath = [Environment]::GetFolderPath("Desktop")
}

$penApiPath = "C:\Users\Hoggit\DCSUTILS\PenAPInet"

$errorReporter = "C:\Users\hoggit\DCSUTILS\Server_Scripts\WebhookAlerter\webhookAlerter.ps1"

$DCSexe = $DCSPath + "\bin\DCS.exe"
$SRSexe = $SRSPath + "\SR-Server.exe"

$MonitoredProcesses = @{
    #"$adminToolsPath\Admin_Bridge.exe" = "$adminToolsPath\Admin_Bridge.exe"
    "$adminToolsPath\Statepump.exe" = "$adminToolsPath\Statepump.exe"
    "$adminToolsPath\Statesman.exe" = "$adminToolsPath\Statesman.exe"
    "$penApiPath\statsAPI.exe"      = "$penApiPath\statsAPI.exe"
    "C:\lso\lso.exe"                = "C:\lso\lso.bat"
}

function StartSRS {
    if (!(Get-Process | where-object { $_.path -eq $SRSexe })) {
        if (Test-Path ($SRSexe + ".new")) {
            $SRSoldDate = (Get-Item $SRSexe).LastWriteTimeUtc
            $SRSnewDate = (Get-Item (Get-Item ($SRSexe + ".new")).Target).LastWriteTimeUtc
            if ($SRSnewDate -gt $SRSoldDate) {
                Move-Item -Force "$SRSexe" ($SRSexe + ".bak")
                Copy-Item ($SRSexe + ".new") $SRSexe
            }
        }
        write-host "Starting SRS"
        & $SRSexe
    }
}

function StopSRS {
    $srsProcess = (Get-Process | where-object { $_.path -eq $SRSexe })
    if ($srsProcess) {
        Write-Host "Stopping SRS"
        Stop-Process $srsProcess
        Start-Sleep 2
    }
}

function StartMonitoring {
    foreach ($ps in $MonitoredProcesses.GetEnumerator()) {
        if (!(Get-Process | where-object { $_.path -eq $ps.Key })) {
            Start-Process -FilePath $ps.Value -WindowStyle Minimized -WorkingDirectory (Get-Item $ps.Value).Directory
        }
    }
    StartSRS
}

function ReStartMonitoring {
    foreach ($ps in $MonitoredProcesses.GetEnumerator()) {
        $monProcess = (Get-Process | where-object { $_.path -eq $ps.Key })
        if ($monProcess) {
            Stop-Process $monProcess
            Start-Sleep 2
        }
        Start-Process -FilePath $ps.Value -WindowStyle Minimized -WorkingDirectory (Get-Item $ps.Value).Directory
    }
    StopSRS
    StartSRS
}

$DCSLogPath = Join-Path $DCSSavedPath "Logs"

function LogRotate {
    $TheTime = (Get-Date -Format s) -replace ":", ""
    Compress-Archive -Path (Join-Path $DCSLogPath "dcs.log") -DestinationPath (Join-Path $DCSLogPath ("dcs.log-" + $TheTime + ".zip"))
}

function StartDCS {
    write-host "Starting DCS"
    # LogRotate
    ReStartMonitoring
    disableFirewall
    & $DCSexe --server --norender
    
    Start-Sleep 75
    enableFirewall
}

#Called when a crash is detected. You can write out to a log or call a discord webhook.
function onCrash {
    if (test-path $errorReporter) {
        if ($hangcount -ge $maxHangcount) {
            & $errorReporter Error "ServerRestarter" "DCS frozen, restarting..."
        }
        elseif ($wasRunning) {
            & $errorReporter Warning "ServerRestarter" "DCS isn't running, restarting..."
        }
    }
}

#Enables the firewall to prevent incoming DCS connections. You need a DCS firewall rule or this will fail.
#Clear this function out if you don't want to have firewall capability. See the README for why.
function enableFirewall {
    write-host "Enable the firewall rule"
    #Old powershell
    #netsh advfirewall firewall set rule name="DCS" new enable=yes
    #New powershell
    Enable-NetFirewallRule -DisplayName "DCStcp"
    Enable-NetFirewallRule -DisplayName "DCSudp"
}

#Disables the firewall to allow incoming DCS connections. You need a DCS firewall rule or this will fail.
#Clear this function if you don't want firewall capability. See the README for why.
function disableFirewall {
    #Old powershell
    #netsh advfirewall firewall set rule name="DCS" new enable=no
    #New powershell
    Disable-NetFirewallRule -DisplayName "DCStcp"
    Disable-NetFirewallRule -DisplayName "DCSudp"
    write-host "Firewall rule disabled"
}

#Checks to see if there's a new Statesman/Statespump waiting to be updated, and if so kills the process and udpates
function updateStatesman {
    if (Test-Path ("$adminToolsPath\Statepump.exe.new")) {
        Stop-Process -Name "Statepump"
        Start-Sleep 2
        Move-Item -Force "$adminToolsPath\Statepump.exe.new" "$adminToolsPath\Statepump.exe"
    }

    if (Test-Path ("$adminToolsPath\Statesman.exe.new")) {
        Stop-Process -Name "Statesman"
        Start-Sleep 2
        Move-Item -Force "$adminToolsPath\Statesman.exe.new" "$adminToolsPath\Statesman.exe"
    }
}

$maxHangcount = 6
$waitTime = 30
$waitTimeLoginFailed = 600

$LogPath = join-path (Get-Location) "Restarter.log"
$hangcount = 0
$wasRunning = $false
# StartMonitoring

while ($true) {
    updateStatesman
    StartMonitoring
    $process = Get-Process DCS -ea SilentlyContinue
    if ($process.Responding -and ($process.MainWindowTitle -eq "Login Failed" -or $process.MainWindowTitle -eq "Login session has expired")) {
        Write-Host "Login Failed detected sleeping for $waitTimeLoginFailed seconds"
        Start-Sleep $waitTimeLoginFailed
        Stop-Process $process -Force
        Start-Sleep 25
        StartDCS
    }
    elseif ($process.Responding) {
        write-host "DCS found and running. Checking again in $waitTime seconds."

        if ($hangcount -gt 0) {
            Add-Content -Path $LogPath -Value ("Recovered at " + (Get-Date -Format s) + " after $hangcount")
        }

        $hangcount = 0
        $wasRunning = $true
        Start-Sleep $waitTime
    }
    elseif ($hangcount -ge $maxHangcount) {
        write-host "DCS is not responding, restarting it"
        Stop-Process $process -Force
        onCrash
        Start-Sleep 25
        $hangcount = 0
        StartDCS
    }
    elseif ($process -and $hangcount -lt 3) {
        write-host "DCS found, but not responding"
        $hangcount++
        Start-Sleep $waitTime
    }
    elseif ($process -and $hangcount -gt 0) {
        $hangcount++
        write-host "DCS still not responding. Hang counter at $hangcount"
        Start-Sleep $waitTime
    }
    else {
        write-host "DCS is not running.."
        if ($wasRunning) {
            write-host ("Looks like we crashed... calling onCrash.")
            onCrash
            $wasRunning = $false
        }
        StartDCS
    }
}

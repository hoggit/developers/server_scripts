# ServerRestartScript
Monitors the DCS.exe application and restarts it if it fails.

This script will run constantly on your DCS server. If the DCS.exe process stops for whatever reason, then this script will restart it.

This script is best used in conjunction with [Ciribob's dedicated server script](https://forums.eagle.ru/showthread.php?t=160829) so that when DCS is restarted, so is your server's mission.

## Configuration

### onCrash
This function will be called when we detect that DCS is either not responding and requires a paddlin', or if the DCS process was running at one point but now it's not.

You can do whatever you'd like here as long as it doesn't crash the script. Some ideas would be to send a discord webhook using `Invoke-RestMethod` or outputting to a file the time/date of the crash.

This is called _before_ starting DCS again, so if you want to gather logs here before dcs overwrites them, you can.

### Firewalls

DCS has had an issue where if people try to connect to your server while it's still loading, it will cause DCS to crash again. We've solved this on our servers by adding a firewall rule which we can enable and disable. Enable the rule blocks traffic on the DCS port, and disabling it allows traffic again. We've named the rule "DCS" and it is controlled by this script.

If you do not want firewall management done by this script, empty out the `enableFirewall` and `disableFirewall` functions and leave them with no executable code.

### Polling time

The script will default to checking every 30 seconds, change the `$waitTime` variable to your desired frequency.

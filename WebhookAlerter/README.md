# Webhook Alerter

This script will push the given message to a webhook url.

## Configuration

Put desired webhook url in the environment variable `webhook_alerter_url`

## Usage

.\webhookAlerter.ps1 \<Message Level> \<Source Script> \<Message>

Valid message levels are:

- Info
- Warning
- Error

--[[
--  DCS Server Poison Script
--  Written by LazyBoot for Hoggit
--
--  See the README.md for more details.
--
--  Configuration:
--
--  restartsBeforeDeath -- The number of times a mission change occurs before we kill the process.
--
]]

local restartsBeforeDeath = 3


local hoggitRestarter = {}
hoggitRestarter.restartCount = 0;

function hoggitRestarter.onNetMissionChanged(newMiz)
    hoggitRestarter.restartCount = hoggitRestarter.restartCount + 1;
    log.write('HoggitRestarter', log.INFO, 'onNetMissionChanged: ' .. hoggitRestarter.restartCount);
    if hoggitRestarter.restartCount > restartsBeforeDeath then
        log.write('HoggitRestarter', log.INFO, 'restart #' .. hoggitRestarter.restartCount .. ', quitting DCS');
        hoggitRestarter.emptyTheServer()
        DCS.exitProcess();
    end
end

function hoggitRestarter.emptyTheServer()
    for id, data in pairs(net.get_player_list()) do
        if id ~= net.get_server_id() then 
            net.kick(id, "Server is restarting.")
        end
    end
end

DCS.setUserCallbacks(hoggitRestarter)
log.write('HoggitRestarter', log.INFO, 'hoggitRestarter loaded.')

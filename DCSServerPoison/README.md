# DCS Server Poison

This script is a DCS Lua hook that will kill the DCS Process after a certain number of mission restarts.

### Installation

Drop the hook into your `Saved Games/DCS.openbeta/Scripts/Hooks` directory.

### Configuration

The following can be configured in the script:

| Config | Default | Description |
|:---|---:|---:|
|`restartsBeforeDeath`|3|This is the number of times a mission changes before we kill the process.<br/><sub><b>Note:</b> A number of 0 or below will kill your server as it tries to load up the first mission</b></sub>


### Logging

We log INFO level to dcs.log. We log both when we detect a restart, and again when we're about to kill the dcs process.

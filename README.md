# Hoggit Server Scripts

A collection of scripts that get used on our DCS servers. Check in each folder for the respective documentation


## ServerRestartScript

Handles keeping the DCS.exe process alive, and associated firewalling that you should be doing when you're bringing your server up.

## DCS Server Poison

This will kill the DCS process automatically after a certain number of mission restarts. It's intended to be used in tandem with something like `ServerRestartScript` which will start the process back up automatically

## SRS Required

This is a hook that will make sure that players are connected to SRS before slotting in to any planes on your server. 